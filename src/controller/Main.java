package controller;

import model.MapReader;
import model.MarsRover;
import view.BoardView;

/**
 * The Class Main.
 * 
 * @author Michael Pavich, Joshua Pefinis, Justin McConnell
 */
public class Main {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		var mapReader = new MapReader();
		var qTable = mapReader.readMapFile("MarsTerrainMap1.csv");
		var rover = new MarsRover(qTable);
		BoardView bView = new BoardView(qTable.getWidth(), qTable.getHeight(), rover);
		bView.initializeGui();
		var isDone = false;
		while (!isDone) {
			rover.runEpisode(1000);

			rover.printQPolicy();

			isDone = true;
		}
	}
}
