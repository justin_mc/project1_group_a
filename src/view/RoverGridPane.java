package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import model.MarsRover;

public class RoverGridPane extends JPanel {

	private static final long serialVersionUID = 1L;
	private int columnCount;
	private int rowCount;
	private List<Rectangle> cells;
	private Point selectedCell;
	private int xOffset = 1;
	private int yOffset = 1;
	private MarsRover rover;

	public RoverGridPane(int width, int height, MarsRover rover) {

		this.columnCount = width;
		this.rowCount = height;
		this.rover = rover;
		this.cells = new ArrayList<>(this.columnCount * rowCount);
		this.addListener();
	}

	private void addListener() {
		MouseAdapter mouseHandler;
		mouseHandler = new MouseAdapter() {
			@Override
			public void mouseMoved(MouseEvent event) {
				int width = getWidth();
				int height = getHeight();
				int cellWidth = width / columnCount;
				int cellHeight = height / rowCount;
				if (RoverGridPane.this.getRover().getOccupiedState() == null) {
					System.out.println("NULL");
				}
				RoverGridPane.this.selectedCell = null;
				if (RoverGridPane.this.getRover().getOccupiedState().getWidth() >= xOffset
						&& RoverGridPane.this.getRover().getOccupiedState().getHeight() >= yOffset) {
					int column = (RoverGridPane.this.getRover().getOccupiedState().getWidth() - xOffset) / cellWidth;
					int row = (RoverGridPane.this.getRover().getOccupiedState().getHeight() - yOffset) / cellHeight;
					if (column >= 0 && row >= 0 && column < columnCount && row < rowCount) {
						RoverGridPane.this.selectedCell = new Point(column, row);
					}
				}
				repaint();
			}
		};
		this.addMouseMotionListener(mouseHandler);
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(800, 800);
	}

	@Override
	public void invalidate() {
		RoverGridPane.this.cells.clear();
		RoverGridPane.this.selectedCell = null;
		super.invalidate();
	}

	@Override
	protected void paintComponent(Graphics graphic) {
		super.paintComponent(graphic);
		Graphics2D g2d = (Graphics2D) graphic.create();
		int width = getWidth();
		int height = getHeight();
		int cellWidth = width / columnCount;
		int cellHeight = height / rowCount;
		int xOffset = (width - (RoverGridPane.this.columnCount * cellWidth)) / 2;
		int yOffset = (height - (RoverGridPane.this.rowCount * cellHeight)) / 2;
		if (RoverGridPane.this.cells.isEmpty()) {
			for (int row = 0; row < rowCount; row++) {
				for (int col = 0; col < columnCount; col++) {
					Rectangle cell = new Rectangle(xOffset + (col * cellWidth), yOffset + (row * cellHeight), cellWidth,
							cellHeight);
					RoverGridPane.this.cells.add(cell);
				}
			}
		}
		if (RoverGridPane.this.selectedCell != null) {

			int index = RoverGridPane.this.getRover().getOccupiedState().getWidth()
					+ (RoverGridPane.this.getRover().getOccupiedState().getHeight() * columnCount);
			Rectangle cell = RoverGridPane.this.cells.get(index);
			g2d.setColor(Color.BLUE);
			g2d.fill(cell);

		}
		g2d.setColor(Color.GRAY);
		for (Rectangle cell : RoverGridPane.this.cells) {
			g2d.draw(cell);
		}
		g2d.dispose();
	}

	/**
	 * Gets the rover.
	 *
	 * @return the rover
	 */
	public MarsRover getRover() {
		return this.rover;
	}
}
