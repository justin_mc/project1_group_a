package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.UIManager;

import model.MarsRover;

public class BoardView {

	private int width;
	private int height;
	private RoverGridPane gridPane;
	private MarsRover rover;

	public BoardView(int width, int height, MarsRover rover) {
		this.width = width;
		this.height = height;
		this.rover = rover;
	}

	public final void initializeGui() {

		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (Exception e) {
				}

				JFrame frame = new JFrame("Testing");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setLayout(new BorderLayout());
				BoardView.this.gridPane = new RoverGridPane(BoardView.this.getWidth(), BoardView.this.getHeight(),
						BoardView.this.rover);
				frame.add(BoardView.this.gridPane);
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
	}

	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}
}
