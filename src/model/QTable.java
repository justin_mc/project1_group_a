package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * The Class QTable.
 * 
 * @author Michael Pavich, Joshua Pefinis, Justin McConnell
 */
public class QTable {

	private HashMap<BoardState, HashMap<BoardState, Double>> qValues;
	private int width;
	private int height;
	private int seed;
	private static final String NORTH = "N:";
	private static final String EAST = "E:";
	private static final String SOUTH = "S:";
	private static final String WEST = "W:";

	/**
	 * Instantiates a new q table.
	 *
	 * @param seed the seed
	 */
	public QTable(int seed) {
		this.qValues = new HashMap<BoardState, HashMap<BoardState, Double>>();
		this.width = 0;
		this.height = 0;
		this.seed = seed;
	}

	/**
	 * Adds the state.
	 *
	 * @param state the state
	 */
	public void addState(BoardState state) {
		this.qValues.put(state, new HashMap<BoardState, Double>());
	}

	/**
	 * Adds the state action pair.
	 *
	 * @param initialState the initial state
	 * @param actionState  the action state
	 */
	public void addStateActionPair(BoardState initialState, BoardState actionState) {
		this.qValues.get(initialState).put(actionState, 0.0);
	}

	/**
	 * Update Q value.
	 *
	 * @param initialState the initial state
	 * @param actionState  the action state
	 * @param alpha        the alpha
	 * @param gamma        the gamma
	 * @param epsilon      the epsilon
	 */
	public void updateQValue(BoardState initialState, BoardState actionState, double alpha, double gamma,
			double epsilon) {
		if (this.containsStateActionPair(initialState, actionState)) {
			var oldQ = this.qValues.get(initialState).get(actionState);

			var newQ = (1 - alpha) * oldQ + alpha * (actionState.getReward()
					+ gamma * this.qValues.get(actionState).get(this.findOptimalQValueActionState(actionState)));

			this.qValues.get(initialState).put(actionState, newQ);
		} else {
			this.addStateActionPair(initialState, actionState);
		}
	}

	/**
	 * Contains state action pair.
	 *
	 * @param initialState the initial state
	 * @param actionState  the action state
	 * @return true, if successful
	 */
	public boolean containsStateActionPair(BoardState initialState, BoardState actionState) {
		if (!this.qValues.containsKey(initialState)) {
			return false;
		}
		if (!this.qValues.get(initialState).containsKey(actionState)) {
			return false;
		}

		return true;
	}

	/**
	 * Gets the random action state.
	 *
	 * @param initialState the initial state
	 * @return the random action state
	 */
	public BoardState getRandomActionState(BoardState initialState) {
		Random random = new Random();
		return (BoardState) this.qValues.get(initialState).keySet().toArray()[random
				.nextInt(this.qValues.get(initialState).keySet().size())];
	}

	/**
	 * Find optimal Q value action state.
	 *
	 * @param initialState the initial state
	 * @return the board state
	 */
	public BoardState findOptimalQValueActionState(BoardState initialState) {
		var bestStates = new ArrayList<BoardState>();
		bestStates.add((BoardState) this.qValues.get(initialState).keySet().toArray()[0]);

		for (var curStateEntry : this.qValues.get(initialState).entrySet()) {
			if (curStateEntry.getValue().doubleValue() > this.qValues.get(initialState).get(bestStates.get(0))
					.doubleValue()) {
				bestStates.clear();
				bestStates.add(curStateEntry.getKey());
			} else if (curStateEntry.getValue().doubleValue() == this.qValues.get(initialState).get(bestStates.get(0))
					.doubleValue()) {
				bestStates.add(curStateEntry.getKey());
			}
		}
		if (bestStates.size() == 1) {
			return bestStates.get(0);
		} else {
			var random = new Random();
			return bestStates.get(random.nextInt(bestStates.size()));
		}
	}

	/**
	 * Find optimal short term reward action state.
	 *
	 * @param initialState the initial state
	 * @return the board state
	 */
	public BoardState findOptimalShortTermRewardActionState(BoardState initialState) {
		var bestStates = new ArrayList<BoardState>();
		bestStates.add((BoardState) this.qValues.get(initialState).keySet().toArray()[0]);

		for (var curState : this.qValues.get(initialState).keySet()) {
			if (curState.getReward() > bestStates.get(0).getReward()) {
				bestStates.clear();
				bestStates.add(curState);
			} else if (curState.getReward() == bestStates.get(0).getReward()) {
				bestStates.add(curState);
			}
		}

		if (bestStates.size() == 1) {
			return bestStates.get(0);
		} else {
			var random = new Random();
			return bestStates.get(random.nextInt(bestStates.size()));
		}
	}

	/**
	 * Connect states.
	 */
	public void connectStates() {
		for (var curState : this.qValues.keySet()) {
			if (curState.getWidth() != 0) {
				this.qValues.get(curState).put(this.findStateAtPosition(curState.getWidth() - 1, curState.getHeight()),
						0.0);
			}
			if (curState.getWidth() != this.width - 1) {
				this.qValues.get(curState).put(this.findStateAtPosition(curState.getWidth() + 1, curState.getHeight()),
						0.0);
			}
			if (curState.getHeight() != 0) {
				this.qValues.get(curState).put(this.findStateAtPosition(curState.getWidth(), curState.getHeight() - 1),
						0.0);
			}
			if (curState.getHeight() != this.height - 1) {
				this.qValues.get(curState).put(this.findStateAtPosition(curState.getWidth(), curState.getHeight() + 1),
						0.0);
			}
		}
	}

	/**
	 * Prints the policy.
	 *
	 * @return the int
	 */
	public int printPolicy() {
		var goalReached = false;
		var curState = this.getStartState();
		var walkCount = 0;
		while (!goalReached) {
			System.out.print(walkCount + " : " + curState + " ; ");
			String north = NORTH;
			String south = SOUTH;
			String east = EAST;
			String west = WEST;
			for (var curAction : this.qValues.get(curState).entrySet()) {
				if (curAction.getKey().getHeight() > curState.getHeight()) {
					south += this.qValues.get(curState).get(curAction.getKey());
				}
				if (curAction.getKey().getHeight() < curState.getHeight()) {
					north += this.qValues.get(curState).get(curAction.getKey());
				}
				if (curAction.getKey().getWidth() > curState.getWidth()) {
					east += this.qValues.get(curState).get(curAction.getKey());
				}
				if (curAction.getKey().getWidth() < curState.getWidth()) {
					west += this.qValues.get(curState).get(curAction.getKey());
				}
			}
			System.out.println(north + " , " + south + " , " + east + " , " + west);
			curState = this.findOptimalQValueActionState(curState);
			walkCount++;
			goalReached = this.isGoalState(goalReached, curState, walkCount);
			goalReached = this.isNegativeState(goalReached, curState, walkCount);
		}
		return walkCount;
	}

	private boolean isGoalState(boolean goalReached, BoardState curState, int walkCount) {
		if (curState.getStateType().equals(StateType.Goal)) {
			System.out.println(walkCount + " : " + curState);
			goalReached = true;
		}
		return goalReached;
	}

	private boolean isNegativeState(boolean goalReached, BoardState curState, int walkCount) {
		if (curState.getReward() <= -50) {
			System.out.println(walkCount + " : " + curState);
			goalReached = true;
		}
		return goalReached;
	}

	/**
	 * Find state at position.
	 *
	 * @param x the x
	 * @param y the y
	 * @return the board state
	 */
	public BoardState findStateAtPosition(int x, int y) {
		for (var curState : this.qValues.keySet()) {
			if (curState.getWidth() == x && curState.getHeight() == y) {
				return curState;
			}
		}
		return null;
	}

	/**
	 * Gets the start state.
	 *
	 * @return the start state
	 */
	public BoardState getStartState() {
		for (var curState : this.qValues.keySet()) {
			if (curState.getStateType().equals(StateType.Start)) {
				return curState;
			}
		}
		return null;
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth() {
		return this.width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public int getHeight() {
		return this.height;
	}

	/**
	 * Sets the width.
	 *
	 * @param width the new width
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * Sets the height.
	 *
	 * @param height the new height
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * Gets the seed.
	 *
	 * @return the seed
	 */
	public int getSeed() {
		return this.seed;
	}

	/**
	 * Prints the values.
	 */
	public void printValues() {
		for (var curState : this.qValues.entrySet()) {
			System.out.print(curState.getKey() + " ; ");
			String north = NORTH;
			String south = SOUTH;
			String east = EAST;
			String west = WEST;
			for (var curAction : this.qValues.get(curState.getKey()).entrySet()) {
				if (curAction.getKey().getHeight() > curState.getKey().getHeight()) {
					south += this.qValues.get(curState.getKey()).get(curAction.getKey());
				}
				if (curAction.getKey().getHeight() < curState.getKey().getHeight()) {
					north += this.qValues.get(curState.getKey()).get(curAction.getKey());
				}
				if (curAction.getKey().getWidth() > curState.getKey().getWidth()) {
					east += this.qValues.get(curState.getKey()).get(curAction.getKey());
				}
				if (curAction.getKey().getWidth() < curState.getKey().getWidth()) {
					west += this.qValues.get(curState.getKey()).get(curAction.getKey());
				}
			}
			System.out.println(north + " , " + south + " , " + east + " , " + west);
		}
	}

	/**
	 * Gets the state above.
	 *
	 * @param state the state
	 * @return the state above
	 */
	public BoardState getStateAbove(BoardState state) {
		for (var curState : this.qValues.keySet()) {
			if (curState.getWidth() == state.getWidth()) {
				if (curState.getHeight() == state.getHeight() - 1) {
					return curState;
				}
			}
		}
		return null;
	}
}
