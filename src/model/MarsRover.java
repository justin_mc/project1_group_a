package model;

import java.util.Random;

/**
 * The Class MarsRover.
 * 
 * @author Michael Pavich, Joshua Pefinis, Justin McConnell
 */
public class MarsRover {

	private final double alpha = 0.5;
	private final double gamma = 0.9;
	private double epsilon = 0;
	private QTable qTable;
	private BoardState lastState;
	private BoardState occupiedState;
	private int episodeCount;

	/**
	 * Instantiates a new mars rover.
	 *
	 * @param qValues the q values
	 */
	public MarsRover(QTable qValues) {
		this.qTable = qValues;
		this.lastState = null;
		this.occupiedState = this.qTable.getStartState();
		this.episodeCount = 0;
	}

	/**
	 * Sets the start.
	 *
	 * @param state the new start
	 */
	public void setStart(BoardState state) {
		this.occupiedState = state;
	}

	/**
	 * Move the rover.
	 */
	public void move() {
		this.lastState = this.occupiedState;

		Random random = new Random();
		if (random.nextDouble() >= this.epsilon) {
			this.occupiedState = this.qTable.findOptimalQValueActionState(this.lastState);
		} else {
			this.occupiedState = this.qTable.getRandomActionState(this.lastState);
		}

		this.qTable.updateQValue(this.lastState, this.occupiedState, this.alpha, this.gamma, this.epsilon);
	}

	/**
	 * Run episode.
	 *
	 * @return the string
	 */
	public String runEpisode() {
		String result = "";

		boolean hasFoundGoal = false;
		while (!hasFoundGoal) {
			this.move();
			result += this.occupiedState + System.lineSeparator();

			if (this.occupiedState.getStateType().equals(StateType.Goal)) {
				this.moveToStartState();
				hasFoundGoal = true;
			}

			if (this.occupiedState.getReward() <= -50) {
				this.moveToStartState();
				hasFoundGoal = true;
			}
		}

		this.episodeCount++;
		return result;
	}

	/**
	 * Run episode.
	 *
	 * @param number the number
	 * @return the string
	 */
	public String runEpisode(int number) {
		String result = "";
		for (int i = 0; i < number; i++) {
			result += this.runEpisode();
		}
		return result;
	}

	/**
	 * Run episode up first.
	 *
	 * @return the string
	 */
	public String runEpisodeUpFirst() {
		String result = "";

		boolean hasFoundGoal = false;
		int count = 1;
		while (!hasFoundGoal) {
			result += count;

			if (count == 0) {
				this.moveUp();
				result += this.occupiedState + System.lineSeparator();
				count++;
			} else {
				this.move();
				result += this.occupiedState + System.lineSeparator();
			}

			count++;

			if (this.occupiedState.getStateType().equals(StateType.Goal)) {
				this.moveToStartState();
				hasFoundGoal = true;
			}

			if (this.occupiedState.getReward() <= -50) {
				this.moveToStartState();
				hasFoundGoal = true;
			}
		}

		return result;
	}

	private boolean moveUp() {
		if (this.occupiedState.getHeight() > 0) {
			this.lastState = this.occupiedState;
			this.occupiedState = this.qTable.getStateAbove(this.lastState);
			this.qTable.updateQValue(this.lastState, this.occupiedState, this.alpha, this.gamma, this.epsilon);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Move to start state.
	 */
	public void moveToStartState() {
		this.occupiedState = this.qTable.getStartState();
	}

	/**
	 * Gets the occupied state.
	 *
	 * @return the occupied state
	 */
	public BoardState getOccupiedState() {
		return this.occupiedState;
	}

	/**
	 * Sets the occupied state.
	 *
	 * @param occupiedState the new occupied state
	 */
	public void setOccupiedState(BoardState occupiedState) {
		this.occupiedState = occupiedState;
	}

	/**
	 * Gets the last state.
	 *
	 * @return the last state
	 */
	public BoardState getLastState() {
		return this.lastState;
	}

	/**
	 * Sets the last state.
	 *
	 * @param lastState the new last state
	 */
	public void setLastState(BoardState lastState) {
		this.lastState = lastState;
	}

	/**
	 * Gets the epsilon.
	 *
	 * @return the epsilon
	 */
	public double getEpsilon() {
		return this.epsilon;
	}

	/**
	 * Sets the epsilon.
	 *
	 * @param epsilon the new epsilon
	 */
	public void setEpsilon(double epsilon) {
		this.epsilon = epsilon;
	}

	/**
	 * Prints the Q policy.
	 *
	 * @return the int
	 */
	public int printQPolicy() {
		return this.qTable.printPolicy();

	}

	/**
	 * Gets the episodes count.
	 *
	 * @return the episodes count
	 */
	public int getEpisodesCount() {
		return this.episodeCount;
	}

	/**
	 * Prints the Q table.
	 */
	public void printQTable() {
		this.qTable.printValues();

	}
}
