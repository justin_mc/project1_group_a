package model;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class BoardState.
 * 
 * @author Michael Pavich, Joshua Pefinis, Justin McConnell
 */
public class BoardState {

	private int xCoordinate;
	private int yCoordinate;
	private int reward;
	private StateType stateType;
	private Map<BoardState, Double> adjacentStates;

	/**
	 * Instantiates a new board state.
	 *
	 * @param x         the x
	 * @param y         the y
	 * @param reward    the reward
	 * @param stateType the state type
	 */
	public BoardState(int x, int y, int reward, StateType stateType) {
		this.xCoordinate = x;
		this.yCoordinate = y;
		this.reward = reward;
		this.stateType = stateType;
		this.adjacentStates = new HashMap<BoardState, Double>();
	}

	/**
	 * Instantiates a new board state.
	 *
	 * @param reward    the reward
	 * @param stateType the state type
	 */
	public BoardState(int reward, StateType stateType) {
		this.reward = reward;
		this.stateType = stateType;
		this.adjacentStates = new HashMap<BoardState, Double>();
	}

	/**
	 * Adds the adjacent state.
	 *
	 * @param state the state
	 */
	public void addAdjacentState(BoardState state) {
		this.adjacentStates.put(state, 0.0);
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth() {
		return this.xCoordinate;
	}

	/**
	 * Sets the width.
	 *
	 * @param width the new width
	 */
	public void setWidth(int width) {
		this.xCoordinate = width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public int getHeight() {
		return this.yCoordinate;
	}

	/**
	 * Sets the height.
	 *
	 * @param height the new height
	 */
	public void setHeight(int height) {
		this.yCoordinate = height;
	}

	/**
	 * Gets the adjacent states.
	 *
	 * @return the adjacent states
	 */
	public Map<BoardState, Double> getAdjacentStates() {
		return this.adjacentStates;
	}

	/**
	 * Sets the adjacent states.
	 *
	 * @param adjacentStates the adjacent states
	 */
	public void setAdjacentStates(Map<BoardState, Double> adjacentStates) {
		this.adjacentStates = adjacentStates;
	}

	/**
	 * Gets the reward.
	 *
	 * @return the reward
	 */
	public int getReward() {
		return this.reward;
	}

	/**
	 * Sets the reward.
	 *
	 * @param reward the new reward
	 */
	public void setReward(int reward) {
		this.reward = reward;
	}

	/**
	 * Gets the state type.
	 *
	 * @return the state type
	 */
	public StateType getStateType() {
		return this.stateType;
	}

	/**
	 * Sets the state type.
	 *
	 * @param stateType the new state type
	 */
	public void setStateType(StateType stateType) {
		this.stateType = stateType;
	}

	@Override
	public String toString() {
		return "(" + this.xCoordinate + "," + this.yCoordinate + ")";
	}

}
