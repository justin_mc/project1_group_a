package model;

public enum StateType {
	
	Start,
	Goal,
	Intermediate
}
