package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * The Class MapReader.
 * 
 * @author Michael Pavich, Joshua Pefinis, Justin McConnell
 */
public class MapReader {

	private QTable qTable;
	private int currentRow;
	private int currentColumn;

	/**
	 * Instantiates a new map reader.
	 */
	public MapReader() {
		this.currentRow = 0;
		this.currentColumn = 0;
	}

	/**
	 * Read map file.
	 *
	 * @param filePath the file path
	 * @return the q table
	 */
	public QTable readMapFile(String filePath) {
		this.qTable = new QTable(35);

		try {
			this.readFile(filePath);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this.qTable;

	}

	private void readFile(String filePath) throws FileNotFoundException, IOException {
		BufferedReader csvReader = new BufferedReader(new FileReader(filePath));
		boolean hasReadDimensions = false;
		String curRow = "";
		while ((curRow = csvReader.readLine()) != null) {
			String[] data = curRow.split(",");

			if (!hasReadDimensions) {
				this.setTableDimensions(data[0]);
				hasReadDimensions = true;
			} else {
				for (var curString : data) {
					this.qTable.addState(this.readStateFromData(curString));

					this.currentColumn++;
				}
				this.currentColumn = 0;
				this.currentRow++;
			}
		}
		this.currentColumn = 0;
		this.currentRow = 0;
		this.qTable.connectStates();
		csvReader.close();
	}

	private BoardState readStateFromData(String data) {
		if (data.equals("Start")) {
			return new BoardState(this.currentColumn, this.currentRow, 0, StateType.Start);
		} else if (data.equals("LOI+100")) {
			return new BoardState(this.currentColumn, this.currentRow, 100, StateType.Goal);
		} else {
			return new BoardState(this.currentColumn, this.currentRow, Integer.parseInt(data), StateType.Intermediate);
		}
	}

	private void setTableDimensions(String data) {
		data = data.substring(3);
		String[] dimensions = data.split(" x ");

		this.qTable.setWidth(Integer.parseInt(dimensions[1]));
		this.qTable.setHeight(Integer.parseInt(dimensions[0]));

	}
}
